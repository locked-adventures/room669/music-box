#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include "musicbox.hpp"

using namespace std;

static const char CONFIG_FILE[] = "/etc/music_box.conf";

static const char MQTT[] = "mqtt";
static const char BROKER[] = "broker";
static const char CLIENTS[] = "clients";
static const char ME[] = "me";
static const char PIGPIO[] = "pigpio";
static const char ADDRESS[] = "address";
static const char PORT[] = "port";
static const char GPIOS[] = "gpios";
static const char SERVO[] = "servo";
static const char A[] = "a";
static const char Z[] = "z";

int main()
{
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::map;
    typedef std::string str;
    using std::ifstream;
    using nlohmann::json;
    typedef unsigned uns;

    cout << "Reading and parsing config file..." << endl;
    ifstream cfg_stream;
    cfg_stream.exceptions(ifstream::failbit | ifstream::badbit);
    cfg_stream.open(CONFIG_FILE);
    json config = json::parse(cfg_stream);
    cout << "Done." << endl;

    MusicBox musicbox(config[MQTT][BROKER].get<str>(),
                      config[MQTT][CLIENTS][ME].get<str>(),
                      config[PIGPIO][ADDRESS].get<str>(),
                      config[PIGPIO][PORT].get<str>(),
                      config[PIGPIO][GPIOS][SERVO].get<uns>(),
                      config[A].get<uns>(),
                      config[Z].get<uns>());

    musicbox.run();
    return 0;
}
