#include "pigpiopp_exception.h"
#include <pigpiod_if2.h>

namespace pigpiopp {

exception::exception(int errcode)
    : errcode(errcode)
{}

int exception::code() const noexcept {
    return errcode;
}

const char *exception::what() const noexcept {
    return pigpio_error(errcode);
}

}
