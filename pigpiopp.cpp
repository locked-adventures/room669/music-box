#include "pigpiopp.h"
#include <pigpiod_if2.h>
#include <stdexcept>
#include "pigpiopp_exception.h"

using std::move;
using std::runtime_error;

namespace pigpiopp {

using std::string;

typedef client self;

static void throw_if_error(int e) {
    if (e < 0) {
        throw exception(e);
    }
}

self::client() : pi(-1) {}

self::client(client&& rhs) : pi(rhs.pi) {
    rhs.pi = -1;
}

client& self::operator =(client&& rhs) {
    pi = rhs.pi;
    rhs.pi = -1;
    return *this;
}

int self::get_mode(unsigned gpio) {
    int r = ::get_mode(pi, gpio);
    throw_if_error(r);
    return r;
}

void self::set_mode(unsigned gpio, unsigned mode) {
    int r = ::set_mode(pi, gpio, mode);
    throw_if_error(r);
}

void client::set_servo_pulsewidth(unsigned gpio, unsigned pulsewidth)
{
    int r = ::set_servo_pulsewidth(pi, gpio, pulsewidth);
    throw_if_error(r);
}

void self::set_pull_up_down(unsigned gpio, unsigned pud) {
    throw_if_error(::set_pull_up_down(pi, gpio, pud));
}

self::client(const char *addr, const char *port)
    : pi(::pigpio_start(addr, port))
{
    throw_if_error(pi);
}

int self::read(unsigned gpio) {
    int r = gpio_read(pi, gpio);
    throw_if_error(r);
    return r;
}

void self::write(unsigned gpio, unsigned level) {
    int r = gpio_write(pi, gpio, level);
    throw_if_error(r);
}

self::~client()
{
    stop();
}

void self::set_glitch_filter(unsigned user_gpio, unsigned steady) {
    throw_if_error( ::set_glitch_filter(pi, user_gpio, steady) );
}

void self::set_noise_filter(unsigned user_gpio, unsigned steady, unsigned active) {
    throw_if_error( ::set_noise_filter(pi, user_gpio, steady, active) );
}

void self::callback_wrapper(int pi, unsigned user_gpio, unsigned level, uint32_t tick, void *void_fptr) {
    (void) pi;
    const callback_t *fptr = (callback_t*)void_fptr;
    (*fptr)(user_gpio, level, tick);
}

int self::register_callback(unsigned user_gpio, unsigned edge, callback_t callback) {
    callback_ptr_t cb(new callback_t(move(callback)));
    int cbid = ::callback_ex(pi, user_gpio, edge, callback_wrapper, cb.get());
    throw_if_error(cbid);
    callback_map.insert(std::pair<int, callback_ptr_t>{cbid, move(cb)});
    return cbid;
}

void self::cancel_callback(int cbid) {
    throw_if_error( ::callback_cancel(cbid) );
}

void self::cancel_all_callbacks() {
    for (const auto &tuple : callback_map) {
        throw_if_error( ::callback_cancel(tuple.first) );
    }
}

void self::start(const char *addr, const char *port) {
    stop();
    pi = ::pigpio_start(addr, port);
    throw_if_error(pi);
}

void self::stop() {
    if (pi >= 0) {
        cancel_all_callbacks();
        ::pigpio_stop(pi);
        callback_map.clear();
        pi = -1;
    }
}

}
