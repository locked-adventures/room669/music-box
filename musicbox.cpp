#include "musicbox.hpp"

#include <mqtt/connect_options.h>

#include <iostream>

#include <stdexcept>
#include <chrono>
#include <cassert>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>
#include <boost/asio/executor_work_guard.hpp>

#include <chrono>
#include <thread>

namespace _impl {
using namespace boost::placeholders;

using boost::asio::make_work_guard;

using std::tolower;
using std::isalpha;

using std::cout;
using std::cerr;
using std::endl;
using std::runtime_error;

typedef std::recursive_mutex mutex_t;

using std::lock_guard;

using std::chrono::seconds;
using std::chrono::milliseconds;

// using boost::system::error_code;

using std::string;
using std::exception;
using std::vector;
using std::stoi;

using std::shared_ptr;
using std::unique_ptr;
using std::make_shared;
using std::make_unique;

using std::ostringstream;

using std::setfill;
using std::setw;

using std::this_thread::sleep_for;
using std::chrono::milliseconds;
using std::chrono::seconds;

using std::atomic_int;
}

unsigned MusicBox::char_to_pulsewidth(char c) {
    using namespace _impl;
    if (isalpha(c)) {
        c = tolower(c);
        // 'a': 90 degrees, 'z': - 90 degrees;
        return a + (int(c - 'a') * int(z - a) / 25);
    }
    return 0;
}

void MusicBox::on_failure(const mqtt::token &asyncActionToken)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    (void) asyncActionToken;
    cout << "Mqtt: on_failure" << endl;
}

void MusicBox::on_success(const mqtt::token &asyncActionToken)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    (void) asyncActionToken;
    cout << "Mqtt: on_success" << endl;
}

void MusicBox::connected(const mqtt::string &cause)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: connected";
    if (cause.size()) {
        cout << ": " << cause;
    }
    cout << endl;

    try {
        mqtt_client->subscribe(mqtt_client->get_client_id() + "/set/#", 0);
        mqtt_client->publish(mqtt_client->get_client_id() + "/status", "ONLINE", 0, true);
        cout << "Mqtt: Status and riddle states published" << endl;
    }
    catch (mqtt::exception &e) {
        cout << "Mqtt: Cannot publish information: " << e.what() << endl;
    }
}

void MusicBox::connection_lost(const mqtt::string &cause)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: connection lost";
    if (cause.size()) {
        cout << ": " << cause;
    }
    cout << endl;
}

void MusicBox::message_arrived(mqtt::const_message_ptr msg_ptr)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    auto topic_ref = msg_ptr->get_topic_ref();

    string topicActivate = mqtt_client->get_client_id() + "/set/activate";

    cout << "Mqtt: message arrived on topic: " << msg_ptr->get_topic() << endl;

    if (topic_ref.to_string().starts_with(topicActivate)) {
        if (msg_ptr->get_payload_ref().to_string().starts_with("0")) {
            cout << "Deactivate" << endl;
            deactivate();
        }
        else {
            cout << "Activate" << endl;
            activate();
        }
    }
}

void MusicBox::delivery_complete(mqtt::delivery_token_ptr)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: message delivery completed" << endl;
}

void MusicBox::activate()
{
    using namespace _impl;

    if (state < 0) {
        state = 0;
        timer.expires_after(seconds(3));
    }
    else {
        pi.set_servo_pulsewidth(servo, char_to_pulsewidth(NAME[state]));
        state++;
        if (state == sizeof(NAME) - 1) {
            state = -1;
        }
        timer.expires_after(seconds(2));
    }
    cout << "State: " << state << endl;
    shared_ptr<atomic_int> gen_ref = generation;
    int gen = *gen_ref;
    timer.async_wait([gen_ref, gen, this](const boost::system::error_code &e) {
        if (e || *gen_ref != gen)
            return;
        lock_guard<mutex_t> l(m);
        activate();
    });
}

void MusicBox::deactivate()
{
    (*generation)++;
    state = -1;
    timer.cancel();
}

MusicBox::~MusicBox()
{
    using namespace _impl;

    try {
        mqtt_client->publish(mqtt_client->get_client_id() + "/status", "OFFLINE", 0, true)->wait();
        mqtt_client.reset();
    }
    catch (const exception &e) {
        cerr << "Mqtt: Client throwed exception on destruction" << endl;
    }
}

void MusicBox::run()
{
    using namespace _impl;

    {
        lock_guard<mutex_t> l(m);

        cout << "MusicBox::run() entered" << endl;

        cout << "Initializing asynchronous MQTT connection..." << endl;
        mqtt::connect_options options = mqtt::connect_options_builder()
                .automatic_reconnect(true)
                .will(mqtt::message(mqtt_client->get_client_id() + "/status", "FAILED", 0, true))
                .finalize();
        mqtt_client->connect(options, nullptr, *this);
        cout << "Done." << endl;

        cout << "Connecting to pigpio daemon..." << endl;
        pi.start(pigpio_address.data(), pigpio_port.data());
        cout << "Done." << endl;
    }

    auto wg = make_work_guard(ioc);
    ioc.run();

    cout << "MusicBox::run() returns" << endl;
}
