#ifndef ROOMCONTROL_HPP
#define ROOMCONTROL_HPP

#include <mutex>
#include <boost/asio/io_context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <mqtt/async_client.h>
#include <memory>
#include <atomic>
#include "pigpiopp.h"

class MusicBox;

class MusicBox : private mqtt::callback, private mqtt::iaction_listener
{
    mutable std::recursive_mutex m;
    bool exiting;

    pigpiopp::client pi;

    unsigned servo;

    std::optional<mqtt::async_client> mqtt_client;

    std::string pigpio_address;
    std::string pigpio_port;

    std::shared_ptr<std::atomic_int> generation;

    boost::asio::io_context ioc;
    boost::asio::steady_timer timer;

    int state;

    unsigned a;
    unsigned z;

    // iaction_listener interface
    void on_failure(const mqtt::token &asyncActionToken) override;
    void on_success(const mqtt::token &asyncActionToken) override;

    // callback interface
    void connected(const mqtt::string &) override;
    void connection_lost(const mqtt::string &) override;
    void message_arrived(mqtt::const_message_ptr) override;
    void delivery_complete(mqtt::delivery_token_ptr) override;

    void activate();
    void deactivate();

    unsigned char_to_pulsewidth(char c);

    constexpr static char NAME[] = "Julia";

public:
    explicit
    MusicBox(const std::string &mqtt_server_uri,
             const std::string &mqtt_client_id,
             const std::string &pigpio_address,
             const std::string &pigpio_port,
             unsigned servo,
             unsigned a,
             unsigned z)
        : exiting(false)
        , pi()
        , servo(servo)
        , mqtt_client(std::in_place, mqtt_server_uri, mqtt_client_id)
        , pigpio_address(pigpio_address)
        , pigpio_port(pigpio_port)
        , generation(std::make_shared<std::atomic_int>(0))
        , ioc()
        , timer(ioc)
        , state(-1)
        , a(a)
        , z(z)
    {
        mqtt_client->set_callback(*this);
    }

    ~MusicBox();

    void run();
};

#endif // ROOMCONTROL_HPP
