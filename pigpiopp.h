#ifndef PIGPIOPP_H
#define PIGPIOPP_H

#include <string>
#include <functional>
#include <map>
#include <cstdint>
#include <memory>

namespace pigpiopp {

enum {
    RISING_EDGE = 0,
    FALLING_EDGE,
    EITHER_EDGE
};

enum {
    INPUT,
    OUTPUT
};

enum {
    PUD_OFF,
    PUD_DOWN,
    PUD_UP
};

class client
{
public:
    typedef std::function<void(unsigned gpio, unsigned level, std::uint32_t tick)> callback_t;

private:
    typedef std::unique_ptr<callback_t> callback_ptr_t;
    std::map<int, callback_ptr_t> callback_map;
    static void callback_wrapper(int pi, unsigned user_gpio, unsigned level, uint32_t tick, void *fptr);

public:
    int pi;

    /**
     * @brief Keep uninitialized.
     * Calling any method except start() and stop() after this constructor is undefined behaviour.
     */
    client();

    // Non-copyable
    client(const client&) = delete;
    client& operator =(const client&) = delete;

    // Movable
    client(client&& rhs);
    client& operator =(client&& rhs);

    /**
     * @brief Construct client and connect to pigpio daemon.
     * @param addr - pigpiod address (nullptr for default)
     * @param port - pigpiod port (nullptr for default)
     */
    client(const char *addr, const char *port);

    ~client();

    int get_mode(unsigned gpio);

    void set_mode(unsigned gpio, unsigned mode);

    void set_servo_pulsewidth(unsigned gpio, unsigned pulsewidth);

    void set_pull_up_down(unsigned gpio, unsigned pud);

    /**
     * @brief Read gpio value.
     * @param gpio
     * @return gpio value
     */
    int read(unsigned gpio);

    /**
     * @brief Write gpio value.
     * @param gpio
     * @param level
     */
    void write(unsigned gpio, unsigned level);

    /**
     * @brief Set glitch filter.
     * @param user_gpio
     * @param steady - in microseconds
     */
    void set_glitch_filter(unsigned user_gpio, unsigned steady);

    /**
     * @brief Set noise filter.
     * @param user_gpio
     * @param steady - in microseconds
     * @param active - in microseconds
     */
    void set_noise_filter(unsigned user_gpio, unsigned steady, unsigned active);

    /**
     * @brief Register a new callback function.
     * @param user_gpio
     * @param edge
     * @param callback
     * @return callback id
     */
    int register_callback(unsigned user_gpio, unsigned edge, callback_t callback);

    /**
     * @brief Cancel a callback.
     * It does not release the callback ressources.
     * @param cbid - callback id
     */
    void cancel_callback(int cbid);

    /**
     * @brief Cancel all callbacks.
     * It does not release the callback ressources.
     * @param cbid - callback id
     */
    void cancel_all_callbacks();

    /**
     * @brief Connect to the pigpio daemon.
     * @param addr
     * @param port
     */
    void start(const char *addr, const char *port);

    /**
     * @brief Disconnect from pigpio daemon and release callback ressources.
     */
    void stop();
};

}

#endif // PIGPIOPP_H
